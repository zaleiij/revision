from bottle import route, run, template
from os import environ


@route('/multiply/<a>/<b>')
def route_multiply(a, b):
    result = multiply(a, b)
    return template('<b>Result of {{a}} * {{b}} is {{result}}</b>!', a=a, b=b, result=result)

def multiply(a, b):
    return int(a) * int(b)


if __name__=='__main__':
    run(host='localhost', port=8080)